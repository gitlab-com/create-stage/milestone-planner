Rails.application.routes.draw do
  devise_for :users, skip: [:registrations]
  as :user do
    get "users/edit" => "devise/registrations#edit", :as => "edit_user_registration"
    put "users" => "devise/registrations#update", :as => "user_registration"
  end

  get "home/index"
  resources :milestones
  resources :time_offs
  resources :team_members
  resources :labels
  resources :teams

  get "up" => "rails/health#show", :as => :rails_health_check

  devise_scope :user do
    authenticated :user do
      root "home#index", as: :authenticated_root
    end

    unauthenticated do
      root "devise/sessions#new", as: :unauthenticated_root
    end
  end
end
