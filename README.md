# Milestone Planner

GitLab Milestone Planning app as described [here](https://gitlab.com/gitlab-com/create-stage/create-stage/-/issues/84)

## Users

The app has sign up disabled, accounts can be created directly on the Rails console. For development use:

```
rake db:seed
```

See also [this issue](https://gitlab.com/gitlab-com/create-stage/milestone-planner/-/issues/3)
