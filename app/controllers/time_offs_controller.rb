class TimeOffsController < ApplicationController
  before_action :set_time_off, only: %i[show edit update destroy]

  def index
    @time_offs = TimeOff.all
  end

  def show
  end

  def new
    @time_off = TimeOff.new
  end

  def edit
  end

  def create
    @time_off = TimeOff.new(time_off_params)

    if @time_off.save
      redirect_to @time_off, notice: "Time off was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @time_off.update(time_off_params)
      redirect_to @time_off, notice: "Time off was successfully updated.", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @time_off.destroy!
    redirect_to time_offs_url, notice: "Time off was successfully destroyed.", status: :see_other
  end

  private

  def set_time_off
    @time_off = TimeOff.find(params[:id])
  end

  def time_off_params
    params.require(:time_off).permit(:team_member_id, :start_date, :end_date, :days)
  end
end
