class MilestonesController < ApplicationController
  before_action :set_milestone, only: %i[show edit update destroy]

  def index
    @milestones = Milestone.all
  end

  def show
  end

  def new
    @milestone = Milestone.new
  end

  def edit
  end

  def create
    @milestone = Milestone.new(milestone_params)

    if @milestone.save
      redirect_to @milestone, notice: "Milestone was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @milestone.update(milestone_params)
      redirect_to @milestone, notice: "Milestone was successfully updated.", status: :see_other
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @milestone.destroy!
    redirect_to milestones_url, notice: "Milestone was successfully destroyed.", status: :see_other
  end

  private

  def set_milestone
    @milestone = Milestone.find(params[:id])
  end

  def milestone_params
    params.require(:milestone).permit(:milestone, :start_date, :end_date, :last_working_day, :workdays, :weight, :gitlab_id)
  end
end
