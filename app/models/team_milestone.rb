class TeamMilestone < ApplicationRecord
  belongs_to :team
  belongs_to :milestone
  has_many :assignments
  has_many :capacities
end
