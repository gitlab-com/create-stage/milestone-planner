class Milestone < ApplicationRecord
  has_many :team_milestones, dependent: :destroy

  validates :milestone, presence: true
  validates :gitlab_id, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
end
