class TeamMember < ApplicationRecord
  belongs_to :team
  has_many :capacities, dependent: :destroy
  has_many :team_milestones, through: :capacities
  has_many :assignments, dependent: :destroy
  # has_many :team_milestones, through: :assignments

  validates :name, presence: true
  validates :handle, presence: true
  validates :gitlab_id, presence: true
end
