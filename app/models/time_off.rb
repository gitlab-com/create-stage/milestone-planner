class TimeOff < ApplicationRecord
  belongs_to :team_member

  validates :start_date, presence: true
  validates :end_date, presence: true
end
