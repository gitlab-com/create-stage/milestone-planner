class CreateTeamMembers < ActiveRecord::Migration[7.1]
  def change
    create_table :team_members do |t|
      t.references :team, null: false, foreign_key: true
      t.string :name
      t.string :handle
      t.integer :gitlab_id
      t.boolean :active, default: true, null: false

      t.timestamps
    end
  end
end
