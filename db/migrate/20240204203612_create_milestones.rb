class CreateMilestones < ActiveRecord::Migration[7.1]
  def change
    create_table :milestones do |t|
      t.string :milestone
      t.date :start_date
      t.date :end_date
      t.date :last_working_day
      t.integer :workdays
      t.integer :weight
      t.integer :gitlab_id

      t.timestamps
    end
  end
end
