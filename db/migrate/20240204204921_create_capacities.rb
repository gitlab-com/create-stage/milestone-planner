class CreateCapacities < ActiveRecord::Migration[7.1]
  def change
    create_table :capacities do |t|
      t.references :team_member, null: false, foreign_key: true
      t.references :team_milestone, null: false, foreign_key: true
      t.decimal :factor, precision: 2, scale: 1
      t.decimal :pto, precision: 2, scale: 1
      t.decimal :on_call, precision: 2, scale: 1
      t.decimal :weight, precision: 2, scale: 1
      t.integer :adjusted_weight
      t.string :notes

      t.timestamps
    end
  end
end
