class CreateAssignments < ActiveRecord::Migration[7.1]
  def change
    create_table :assignments do |t|
      t.references :team_member, null: false, foreign_key: true
      t.references :team_milestone, null: false, foreign_key: true
      t.string :issue_title
      t.string :issue_url
      t.string :issue_type
      t.integer :weight

      t.timestamps
    end
  end
end
