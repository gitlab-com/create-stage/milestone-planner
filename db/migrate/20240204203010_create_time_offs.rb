class CreateTimeOffs < ActiveRecord::Migration[7.1]
  def change
    create_table :time_offs do |t|
      t.references :team_member, null: false, foreign_key: true
      t.date :start_date
      t.date :end_date
      t.integer :days

      t.timestamps
    end
  end
end
