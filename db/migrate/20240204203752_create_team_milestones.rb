class CreateTeamMilestones < ActiveRecord::Migration[7.1]
  def change
    create_table :team_milestones do |t|
      t.references :team, null: false, foreign_key: true
      t.references :milestone, null: false, foreign_key: true
      t.boolean :planning
      t.boolean :capacity
      t.boolean :assigned
      t.string :planning_issue
      t.string :planning_board
      t.string :build_board

      t.timestamps
    end
  end
end
