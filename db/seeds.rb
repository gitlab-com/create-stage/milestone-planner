require "factory_bot_rails"
require "ffaker"
include FactoryBot::Syntax::Methods # standard:disable Style/MixinUsage

if Rails.env == "development"
  User.create!(
    email: "admin@example.com",
    password: "gitlabrocks!",
    password_confirmation: "gitlabrocks!",
    admin: true,
    manager_name: "Admin"
  )

  create(:team_member)
  create(:team_milestone)
end
