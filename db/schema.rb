# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_02_08_090009) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.bigint "team_member_id", null: false
    t.bigint "team_milestone_id", null: false
    t.string "issue_title"
    t.string "issue_url"
    t.string "issue_type"
    t.integer "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_member_id"], name: "index_assignments_on_team_member_id"
    t.index ["team_milestone_id"], name: "index_assignments_on_team_milestone_id"
  end

  create_table "capacities", force: :cascade do |t|
    t.bigint "team_member_id", null: false
    t.bigint "team_milestone_id", null: false
    t.decimal "factor", precision: 2, scale: 1
    t.decimal "pto", precision: 2, scale: 1
    t.decimal "on_call", precision: 2, scale: 1
    t.decimal "weight", precision: 2, scale: 1
    t.integer "adjusted_weight"
    t.string "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_member_id"], name: "index_capacities_on_team_member_id"
    t.index ["team_milestone_id"], name: "index_capacities_on_team_milestone_id"
  end

  create_table "labels", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_id"], name: "index_labels_on_team_id"
  end

  create_table "milestones", force: :cascade do |t|
    t.string "milestone"
    t.date "start_date"
    t.date "end_date"
    t.date "last_working_day"
    t.integer "workdays"
    t.integer "weight"
    t.integer "gitlab_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_members", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.string "name"
    t.string "handle"
    t.integer "gitlab_id"
    t.boolean "active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_id"], name: "index_team_members_on_team_id"
  end

  create_table "team_milestones", force: :cascade do |t|
    t.bigint "team_id", null: false
    t.bigint "milestone_id", null: false
    t.boolean "planning"
    t.boolean "capacity"
    t.boolean "assigned"
    t.string "planning_issue"
    t.string "planning_board"
    t.string "build_board"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["milestone_id"], name: "index_team_milestones_on_milestone_id"
    t.index ["team_id"], name: "index_team_milestones_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name"
    t.string "project"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_teams_on_user_id"
  end

  create_table "time_offs", force: :cascade do |t|
    t.bigint "team_member_id", null: false
    t.date "start_date"
    t.date "end_date"
    t.integer "days"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["team_member_id"], name: "index_time_offs_on_team_member_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "manager_name"
    t.boolean "admin", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "assignments", "team_members"
  add_foreign_key "assignments", "team_milestones"
  add_foreign_key "capacities", "team_members"
  add_foreign_key "capacities", "team_milestones"
  add_foreign_key "labels", "teams"
  add_foreign_key "team_members", "teams"
  add_foreign_key "team_milestones", "milestones"
  add_foreign_key "team_milestones", "teams"
  add_foreign_key "teams", "users"
  add_foreign_key "time_offs", "team_members"
end
