FactoryBot.define do
  factory :team_milestone do
    team { build :team }
    milestone { build :milestone }
    planning { false }
    capacity { false }
    assigned { false }
    planning_issue { "MyString" }
    planning_board { "MyString" }
    build_board { "MyString" }
  end
end
