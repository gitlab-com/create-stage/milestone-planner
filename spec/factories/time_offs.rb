FactoryBot.define do
  factory :time_off do
    team_member { create :team_member }
    start_date { "2024-02-04" }
    end_date { "2024-02-04" }
    days { 1 }
  end
end
