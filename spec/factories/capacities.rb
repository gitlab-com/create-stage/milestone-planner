FactoryBot.define do
  factory :capacity do
    team_member { build :team_member }
    team_milestone { build :milestone }
    factor { "1.0" }
    pto { "9.5" }
    on_call { "9.5" }
    weight { "9.5" }
    adjusted_weight { 1 }
    notes { "MyString" }
  end
end
