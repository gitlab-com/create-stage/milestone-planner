FactoryBot.define do
  factory :team do
    user { build :user }
    name { "Create: Source Code BE" }
    project { "gitlab-com/create-stage/source-code-be" }
  end
end
