FactoryBot.define do
  factory :team_member do
    team { build :team }
    name { "Sean Carroll" }
    handle { "sean_carroll" }
    gitlab_id { 4391348 }
    active { true }
  end
end
