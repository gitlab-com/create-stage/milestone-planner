FactoryBot.define do
  factory :assignment do
    team_member { build :team_member }
    team_milestone { build :team_milestone }
    issue_title { "Cached counts not validated" }
    issue_url { "https://gitlab.com/gitlab-org/gitlab/-/issues/435914" }
    issue_type { "type::bug" }
    weight { 3 }
  end
end
