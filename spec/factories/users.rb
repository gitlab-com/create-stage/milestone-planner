FactoryBot.define do
  factory :user do
    email { FFaker::Internet.unique.email }
    password { "password" }
    password_confirmation { "password" }
    admin { false }
    manager_name { FFaker::Name.name }
  end
end
