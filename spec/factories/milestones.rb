FactoryBot.define do
  factory :milestone do
    milestone { "16.10" }
    start_date { "2024-02-10" }
    end_date { "2024-03-15" }
    last_working_day { "2024-03-15" }
    workdays { 20 }
    weight { 10 }
    gitlab_id { 3228778 }
  end
end
