require "rails_helper"

RSpec.describe TimeOffsController, type: :controller do
  render_views

  let(:time_off) { create :time_off }

  let(:valid_attributes) {
    FactoryBot.attributes_for(:time_off)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:time_off, start_date: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      get :index

      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      get :show, params: {id: time_off.to_param}

      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new

      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      time_off = TimeOff.create! valid_attributes
      get :edit, params: {id: time_off.to_param}
      expect(response).to be_successful
    end
  end

  #   describe "POST #create" do
  #     context "with valid params" do
  #       it "creates a new Time Off" do
  #         expect {
  #           post :create, params: {time_off: valid_attributes}
  #         }.to change(TimeOff, :count).by(1)
  #       end
  #
  #       it "redirects to the created time_off" do
  #         post :create, params: {time_off: valid_attributes}
  #         expect(response).to redirect_to(TimeOff.last)
  #       end
  #     end
  #
  #     context "with invalid params" do
  #       it "renders a response with 422 status (i.e. to display the 'new' template)" do
  #         post :create, params: {time_off: invalid_attributes}
  #         expect(response).to have_http_status(:unprocessable_entity)
  #       end
  #
  #     end
  #   end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {days: 2}
      }

      it "updates the requested time_off" do
        time_off = TimeOff.create! valid_attributes
        put :update, params: {id: time_off.to_param, time_off: new_attributes}
        time_off.reload

        expect(time_off.days).to eq(2)
      end

      it "redirects to the time_off" do
        time_off = TimeOff.create! valid_attributes
        put :update, params: {id: time_off.to_param, time_off: new_attributes}
        expect(response).to redirect_to(time_off)
      end
    end

    context "with invalid params" do
      it "renders a response with 422 status (i.e. to display the 'edit' template)" do
        time_off = TimeOff.create! valid_attributes
        put :update, params: {id: time_off.to_param, time_off: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested time_off" do
      time_off = TimeOff.create! valid_attributes
      expect {
        delete :destroy, params: {id: time_off.to_param}
      }.to change(TimeOff, :count).by(-1)
    end

    it "redirects to the time_offs list" do
      time_off = TimeOff.create! valid_attributes
      delete :destroy, params: {id: time_off.to_param}
      expect(response).to redirect_to(time_offs_url)
    end
  end
end
