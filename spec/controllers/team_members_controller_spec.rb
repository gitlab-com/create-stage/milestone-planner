require "rails_helper"

RSpec.describe TeamMembersController, type: :controller do
  render_views

  let(:user) { create :user }

  before do
    sign_in user
  end

  let(:valid_attributes) {
    FactoryBot.attributes_for(:team_member)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:team_member, handle: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      TeamMember.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      team_member = TeamMember.create! valid_attributes
      get :show, params: {id: team_member.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      team_member = TeamMember.create! valid_attributes
      get :edit, params: {id: team_member.to_param}
      expect(response).to be_successful
    end
  end

  #   describe "POST #create" do
  #     context "with valid params" do
  #       it "creates a new TeamMember" do
  #         expect {
  #           post :create, params: {team_member: valid_attributes}
  #         }.to change(TeamMember, :count).by(1)
  #       end
  #
  #       it "redirects to the created team_member" do
  #         post :create, params: {team_member: valid_attributes}
  #         expect(response).to redirect_to(TeamMember.last)
  #       end
  #     end
  #
  #     context "with invalid params" do
  #
  #       it "renders a response with 422 status (i.e. to display the 'new' template)" do
  #         post :create, params: {team_member: invalid_attributes}
  #         expect(response).to have_http_status(:unprocessable_entity)
  #       end
  #
  #     end
  #   end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested team_member" do
        team_member = TeamMember.create! valid_attributes
        put :update, params: {id: team_member.to_param, team_member: new_attributes}
        team_member.reload
        skip("Add assertions for updated state")
      end

      it "redirects to the team_member" do
        team_member = TeamMember.create! valid_attributes
        put :update, params: {id: team_member.to_param, team_member: new_attributes}
        expect(response).to redirect_to(team_member)
      end
    end

    context "with invalid params" do
      it "renders a response with 422 status (i.e. to display the 'edit' template)" do
        team_member = TeamMember.create! valid_attributes
        put :update, params: {id: team_member.to_param, team_member: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested team_member" do
      team_member = TeamMember.create! valid_attributes
      expect {
        delete :destroy, params: {id: team_member.to_param}
      }.to change(TeamMember, :count).by(-1)
    end

    it "redirects to the team_members list" do
      team_member = TeamMember.create! valid_attributes
      delete :destroy, params: {id: team_member.to_param}
      expect(response).to redirect_to(team_members_url)
    end
  end
end
