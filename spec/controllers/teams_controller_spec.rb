require "rails_helper"

RSpec.describe TeamsController, type: :controller do
  render_views

  let(:valid_attributes) {
    FactoryBot.attributes_for(:team)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:team, name: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      Team.create! valid_attributes
      get :index, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      team = Team.create! valid_attributes
      get :show, params: {id: team.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      team = Team.create! valid_attributes
      get :edit, params: {id: team.to_param}
      expect(response).to be_successful
    end
  end

  #   describe "POST #create" do
  #     context "with valid params" do
  #       it "creates a new Team" do
  #         expect {
  #           post :create, params: {team: valid_attributes}
  #         }.to change(Team, :count).by(1)
  #       end
  #
  #       it "redirects to the created team" do
  #         post :create, params: {team: valid_attributes}
  #         expect(response).to redirect_to(Team.last)
  #       end
  #     end
  #
  #     context "with invalid params" do
  #
  #       it "renders a response with 422 status (i.e. to display the 'new' template)" do
  #         post :create, params: {team: invalid_attributes}
  #         expect(response).to have_http_status(:unprocessable_entity)
  #       end
  #
  #     end
  #   end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested team" do
        team = Team.create! valid_attributes
        put :update, params: {id: team.to_param, team: new_attributes}
        team.reload
        skip("Add assertions for updated state")
      end

      it "redirects to the team" do
        team = Team.create! valid_attributes
        put :update, params: {id: team.to_param, team: new_attributes}
        expect(response).to redirect_to(team)
      end
    end

    context "with invalid params" do
      it "renders a response with 422 status (i.e. to display the 'edit' template)" do
        team = Team.create! valid_attributes
        put :update, params: {id: team.to_param, team: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested team" do
      team = Team.create! valid_attributes
      expect {
        delete :destroy, params: {id: team.to_param}
      }.to change(Team, :count).by(-1)
    end

    it "redirects to the teams list" do
      team = Team.create! valid_attributes
      delete :destroy, params: {id: team.to_param}
      expect(response).to redirect_to(teams_url)
    end
  end
end
