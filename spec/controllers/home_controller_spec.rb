require "rails_helper"

RSpec.describe HomeController, type: :controller do
  render_views

  let(:user) { create :user }

  before do
    sign_in user
  end

  describe "GET #index" do
    context "when the user does not have any teams" do
      it "returns a success response" do
        get :index

        expect(response).to be_successful
      end
    end

    context "when the user has a teams" do
      let(:team) { create :team, user: user }

      it "returns a success response" do
        get :index

        expect(response).to be_successful
      end
    end
  end
end
