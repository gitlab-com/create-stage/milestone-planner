require "rails_helper"

RSpec.describe MilestonesController, type: :controller do
  render_views

  let(:valid_attributes) {
    FactoryBot.attributes_for(:milestone)
  }

  let(:invalid_attributes) {
    FactoryBot.attributes_for(:milestone, milestone: nil)
  }

  describe "GET #index" do
    it "returns a success response" do
      get :index
      expect(response).to be_successful
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      milestone = Milestone.create! valid_attributes
      get :show, params: {id: milestone.to_param}
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      milestone = Milestone.create! valid_attributes
      get :edit, params: {id: milestone.to_param}
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Milestone" do
        expect {
          post :create, params: {milestone: valid_attributes}
        }.to change(Milestone, :count).by(1)
      end

      it "redirects to the created milestone" do
        post :create, params: {milestone: valid_attributes}
        expect(response).to redirect_to(Milestone.last)
      end
    end

    context "with invalid params" do
      it "renders a response with 422 status (i.e. to display the 'new' template)" do
        post :create, params: {milestone: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested milestone" do
        milestone = Milestone.create! valid_attributes
        put :update, params: {id: milestone.to_param, milestone: new_attributes}
        milestone.reload
        skip("Add assertions for updated state")
      end

      it "redirects to the milestone" do
        milestone = Milestone.create! valid_attributes
        put :update, params: {id: milestone.to_param, milestone: new_attributes}
        expect(response).to redirect_to(milestone)
      end
    end

    context "with invalid params" do
      it "renders a response with 422 status (i.e. to display the 'edit' template)" do
        milestone = Milestone.create! valid_attributes
        put :update, params: {id: milestone.to_param, milestone: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested milestone" do
      milestone = Milestone.create! valid_attributes
      expect {
        delete :destroy, params: {id: milestone.to_param}
      }.to change(Milestone, :count).by(-1)
    end

    it "redirects to the milestones list" do
      milestone = Milestone.create! valid_attributes
      delete :destroy, params: {id: milestone.to_param}
      expect(response).to redirect_to(milestones_url)
    end
  end
end
